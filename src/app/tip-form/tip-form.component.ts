import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Tip } from '../common/Tip';
import { TipService } from '../common/tips.service';

@Component({
  selector: 'app-tip-form',
  templateUrl: './tip-form.component.html',
  styleUrls: ['./tip-form.component.css']
})
export class TipFormComponent implements OnInit {

  addOrEditLabel: string;
  showResetButton = true;

  @Input() visible = false;
  @Input()
   set addOrEdit(addOrEdit: string) {
      this.addOrEditLabel = addOrEdit;
      if (addOrEdit === 'Add') {
        this.showResetButton = true;
      } else {
        this.showResetButton = false;
      }
   }

  // use a set param if we want to do something else if this value is set
  @Input()
  set currentTip(tip: Tip) {
    if (this.tipForm !== undefined) {
      this.tipForm.reset();
      this.tipForm.setValue(tip);
      console.log(tip);
    }
  }

  @Output() formSubmitted = new EventEmitter<{tip: Tip}>();

  // our reactive form
  tipForm: FormGroup;

  constructor(private tipService: TipService) { }

  ngOnInit() {
    // we can add a number of validators (use array format [Validators.required,Validators.email, etc.])
    this.tipForm = new FormGroup ({
      'id': new FormControl(null),
      'title': new FormControl(null, Validators.required),
      'tip': new FormControl(null, Validators.required),
      'url': new FormControl(null, Validators.required),
      'author': new FormControl(null, Validators.required)
    });
  }

  onSubmit() {
    console.log('--------------------------------- submitted tip form ----------------------------------');
    console.log(this.tipForm);

    if (this.tipForm.valid) {
      const submittedTip = this.tipForm.value as Tip;
      this.tipService.addOrUpdateTip(submittedTip);
      this.formSubmitted.emit({ tip: submittedTip });
    } else {
      // trigger all our validators
      // good reference at: https://loiane.com/2017/08/angular-reactive-forms-trigger-validation-on-submit/
      Object.keys(this.tipForm.controls).forEach(
        field => {
          const control = this.tipForm.get(field);
          if (control instanceof FormControl) {
            control.markAsTouched({ onlySelf: true });
          }
        }
      );
      // trigger form validation
      this.tipForm.markAsTouched();
    }

  }

  resetForm() {
    this.tipForm.reset();
  }
}
