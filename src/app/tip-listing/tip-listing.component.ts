import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Tip } from '../common/Tip';
import { TipService } from '../common/tips.service';

@Component({
  selector: 'app-tip-listing',
  templateUrl: './tip-listing.component.html',
  styleUrls: ['./tip-listing.component.css']
})
export class TipListingComponent implements OnInit {

  tips: Array<Tip>;
  tipCount = 0;
  @Input() visible = true;
  @Output() editTipEvent = new EventEmitter<{tip: Tip}>();

  // inject our service here
  constructor(private tipService: TipService) {

    // we subscribe to our service event so we can update the list
    this.tipService.statusUpdated.subscribe(
      (status: string) => this.updateList(status)
    );
  }

  ngOnInit() {
    this.updateList('get tips for the first time');
  }

  editTip(tip: Tip) {
    this.editTipEvent.emit({tip: tip});
  }

  removeTip(tip: Tip) {
    const deleteTip = confirm('Delete this tip?');

    if (deleteTip) {
      console.log('remove tip');
      this.tipService.removeTip(tip);
    }
  }

  updateList(status) {
    this.tips = this.tipService.getTips();
    this.tipCount = this.tips.length;
    console.log(status);
  }
}
