import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-show-tips-button',
  templateUrl: './show-tips-button.component.html',
  styleUrls: ['./show-tips-button.component.css']
})
export class ShowTipsButtonComponent implements OnInit {
  @Input() disabled = false;
  @Output() showTipsEvent = new EventEmitter<{showTips: boolean}>();

  constructor() { }

  ngOnInit() {
  }

  showTips() {
    this.showTipsEvent.emit({showTips: true});
    console.log('emit showTipsEvent');
  }

  getClasses() {
    const classes =  {
        disabled: this.disabled
    };
    return classes;
  }
}
