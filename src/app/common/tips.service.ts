import { EventEmitter, Injectable } from '@angular/core';
import { Tip } from './Tip';

@Injectable()
export class TipService {
  tips = Array<Tip>();

  constructor() {

    // tslint:disable-next-line:max-line-length
    // add some test data - normally we would be grabbing this from a db source through an http call - this is all happening in memory (nothing is permanent!)
    this.tips.push(new Tip(1, 'Joanna khoury', 'Be a SERVANT Leader', `
    People are tempted to associate management with authority and power.  And unfortunately many confuse between management and leadership.
    Traditional Managers: have a managerial title, have subordinates, dictate rules, hire and fire, assess the work, and take all the
    credit! Leaders may not have a managerial title but are definitely a role model, do not have subordinates but colleagues, do not
    necessarily set the rules but do respect the rules, do not assess the work but just help the team to get the work done and may not
    necessarily take any of the credit. Leaders are honest, transparent, helpful, respected, and wise, they put the benefit of their team
    ahead of their own personal benefit. As a scrum master you are a leader.  A Servant leader event, because the main goal of a scrum
    master is to help his/her team achieve their goal, be their mentor, remove impediments and do part of the work with them.
    `, 'https://www.linkedin.com/pulse/five-simple-tips-become-super-scrum-master-joanna-khoury'));

    this.tips.push(new Tip(2, 'Mike Cohn', 'First Be Quiet', `
    Embrace the effectiveness of the all important pause, the enabling power of silence. When you are tempted to speak, waiting first
    for others in that awkward uncomfortable stillness that follows a complicated question or thought-provoking comment. You don’t need
    to have all the answers.
    And while you are mentally silencing yourself, remember to listen actively. Resist the temptation to begin forming a mental reply
    before a team member has finished speaking. Instead, while someone else is talking, give their thoughts your full attention. When
    they’ve finished, think before replying. There’s nothing wrong with being quiet while you decide how to reply.
    Believe me when I say that to become a fully autonomous, self-organizing team, your team needs your silence more than it needs your
    insightful comments.
    `, 'https://www.mountaingoatsoftware.com/blog/eight-tips-to-become-the-scrum-master-your-team-needs'));

    this.tips.push(new Tip(3, 'Mike Cohn', 'Clear a Path', `Try to end each day with all known impediments resolved. That
    doesn’t mean you can necessarily clear every impediment by the end of the day--some impediments take some time and maneuvering
    to remove. But you should make some progress every single day.
    Remember, to be the Scrum Master your team needs you to be, you don’t need to do the work--you need to make the work easier
    for the team to accomplish.
    `, 'https://www.mountaingoatsoftware.com/blog/eight-tips-to-become-the-scrum-master-your-team-needs'));


    this.tips.push(new Tip(4, 'Joanna khoury', 'Be Agile yourself and live the agile values', `
    The agile core values come from the Agile Manifesto: Individuals and interactions, working software, responding to change and
    customer collaboration. Scrum teams are self-organizing teams and therefore all team members should share some common values.
    These are: Openness, courage, respect, commitment, and focus.
    Agile and Scrum teams specifically share empirical thinking principles: Transparency, inspection and adaptation
    I can't see a good scrum master that do not live these values and principles in his/her everyday work, I can't see a good scrum
    master not fully convinced of the importance of being transparent, honest, courageous, respect others and respect commitment, open
    to new ideas, open to change....
    To be great scrum master you need first to believe in these values, second to live them, third to transmit them to your team and
    your company!
    `, 'https://www.linkedin.com/pulse/five-simple-tips-become-super-scrum-master-joanna-khoury'));

    this.tips.push(new Tip(5, 'Tirthankar Barari', 'Bite off only what you can chew', `
    During the sprint planning phase, as the team is committing to backlog items, help them question whether or not they can truly
    cover all the items. Typically, during planning, development team members have at least some tasks and items they know they will
    be working on. Individual team members will often know when they are being overloaded, but your job is to be an extra set of eyes.
    Make sure no one person is taking on too much and that the overall workload seems feasible. Ask questions and challenge assumptions.
    Bringing potential problems to light early helps mitigate the risk of over-promising and under-delivering to some extent.
    `, 'https://www.scrumalliance.org/community/articles/2009/may/tips-for-first-time-scrummasters'));

    this.tips.push(new Tip(6, 'Tirthankar Barari', 'The organization is not for Scrum. Scrum is for the organization', `
    Are we doing 100 percent Scrum 100 percent of the time? After all, according to line 7 of page 17 of this book from that author,
    we should complete this meeting within 3 hours, else we are violating the rules of Scrum. How many times have you, as the ScrumMaster,
    faced these questions, particularly during the early stages? Wish you had a scale to measure your ”Scrumness” that you could display
    on top of your desk or on your wiki page in real time?

    Sometimes we forget the basic premise for doing Scrum. In the end, your customers will not pay the bills on how “Scrummy” you were.
    Certainly it is important to follow the guidelines defined in Scrum, but the ultimate goal is to deliver what you promised, within
    budget and on time. In that respect, Scrum is a great enabler and more of a means to an end rather than an end by itself. While you are
    helping your team follow the process, don’t get so caught up in the nitty-gritty details that lose sight of your organization’s goals.
    `, 'https://www.scrumalliance.org/community/articles/2009/may/tips-for-first-time-scrummasters'));

    this.tips.push(new Tip(7, 'Tirthankar Barari', 'You thought of apples, I gave you oranges', `
    Many times, what the Product Owner when he wrote the user story is not at all what the UI designer or the developer understood it to be.
    Your job is to make sure they are all on the same page as early as possible in the sprint. As a followup to the marathon sprint planning
    meeting, make sure these players stay in sync and in agreement on each backlog item.

    For complex backlog items or features, one of many ways to accomplish this is setup one or more mini demos along the way. These should
    be quick 10-to-15-minute presentations run from the workstation of the designer/architect/developer. Do not involve the whole team for
    this.
    Ideally, this mini demo should include one or two team members who have been working on the backlog item and the Product Owner. No fancy
    slides or preparation time. Keep it very informal. If it is difficult to get the Product Owner and others together for ten or fifteen
    minutes, you can accomplish something similar by circulating screen shots. The point is to keep the lines of communication open and
    receive feedback earlier rather than later.

    Many people may have inhibitions against running mini demos, as opposed to one overarching demo at the end during review. But for large,
    complex features, periodic mini demos can be extremely helpful in catching discrepancies in understanding early on.
    `, 'https://www.scrumalliance.org/community/articles/2009/may/tips-for-first-time-scrummasters'));

    console.log('service constructor called');
  }

  statusUpdated = new EventEmitter<string>();

  addOrUpdateTip(tip: Tip) {
    if (tip.id === 0) {
      // we will crudely increment last id by 1
      const newId = this.tips[this.tips.length - 1].id++;
      tip.id = newId;
      this.tips.push(tip);
    } else {
      const existingTip = this.tips.findIndex(function (t) {
        return t.id === tip.id;
      });

      // if not found .. index returns -1
      if (existingTip !== -1) {
        this.tips[existingTip] = tip; // we cast to Tip type because an object is passed from the form
        console.log('tip updated');
      } else {
        console.log('doesn\'t exist');
      }
    }

    console.log('emit service event updateList');
    this.statusUpdated.emit('updateList');
  }

  removeTip(tip: Tip) {
    const existingTip = this.tips.findIndex(function (t) {
      return t.id === tip.id;
    });

    if (existingTip !== -1) {
      this.tips.splice(existingTip, 1);
      console.log('emit service event deleteTip');
      this.statusUpdated.emit('deleteTip');
    }
  }

  getTips() {
    return this.tips;
  }
}
