export class Tip {
  id: number;
  title: string;
  tip: string;
  author: string;
  url: string;

  // to have overloaded constructor, we need to mark params as optional
  constructor(id?: number, author?: string, title?: string, tip?: string, url?: string) {
    this.id = id;
    this.author = author;
    this.title = title;
    this.tip = tip;
    this.url = url;
  }
}
