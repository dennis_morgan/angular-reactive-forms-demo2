import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-add-tip-button',
  templateUrl: './add-tip-button.component.html',
  styleUrls: ['./add-tip-button.component.css']
})
export class AddTipButtonComponent implements OnInit {

  // output method we expose to our parent component
  @Output() addNewTipEvent = new EventEmitter<{addTip: boolean}>();
  @Input() disabled = false;

  constructor() { }

  ngOnInit() {
  }

  // called when we click the button
  addNewTip() {
    // we emit an event that the parent can respond to
    this.addNewTipEvent.emit({addTip: true});
  }

  getClasses() {
    const classes =  {
        disabled: this.disabled
    };
    return classes;
  }
}
