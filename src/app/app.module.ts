import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { TipListingComponent } from './tip-listing/tip-listing.component';
import { AddTipButtonComponent } from './add-tip-button/add-tip-button.component';
import { ShowTipsButtonComponent } from './show-tips-button/show-tips-button.component';
import { TipFormComponent } from './tip-form/tip-form.component';
import { TipService } from './common/tips.service';

@NgModule({
  declarations: [
    AppComponent,
    TipListingComponent,
    AddTipButtonComponent,
    ShowTipsButtonComponent,
    TipFormComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ReactiveFormsModule,
  ],
  // tslint:disable-next-line:max-line-length
  providers: [TipService], // we define this here .. and if we want the same instance.. we do not need to redefine in child components "providers"
  bootstrap: [AppComponent]
})
export class AppModule { }

// plunker was updated 11:50pm 8-23-2017
