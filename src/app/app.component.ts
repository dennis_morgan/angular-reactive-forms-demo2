import { Component, VERSION } from '@angular/core';
import { Tip } from './common/Tip';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  version = `${VERSION.full}`;
  addNewButtonDisabled = false;
  showTipsButtonDisabled = true;
  currentTip = new Tip(0, '', '', '', '');
  addOrEdit = 'Add';

  tipListingVisible = true;
  tipFormVisible = false;   // TODO: reverse this

  // this event is called when our child component "add-tip-button" is clicked
  onAddNewTip(addNewTipData: { addTip: boolean }) {
    console.log('please respond from main');
    if (addNewTipData.addTip) {
      // disable button and show form
      this.addNewButtonDisabled = true;
      this.showTipsButtonDisabled = false;
      this.tipListingVisible = false;
      this.tipFormVisible = true;
      this.addOrEdit = 'Add';
      this.currentTip = new Tip(0, '', '', '', ''); // blank out our form
    }
  }

  onShowTips(showTipsData: { showTips: boolean }) {
    if (showTipsData.showTips) {
      this.addNewButtonDisabled = false;
      this.showTipsButtonDisabled = true;
      this.tipListingVisible = true;
      this.tipFormVisible = false;
      console.log('show tips');
    }
  }

  onEditTip(tipBeingEdited: {tip: Tip}) {
    console.log('onEditTip called');
    this.currentTip = tipBeingEdited.tip;
    this.tipListingVisible = false;
    this.tipFormVisible = true;   // TODO: reverse this
    this.addOrEdit = 'Edit';
    this.showTipsButtonDisabled = false;
  }

  onFormSubmitted(submittedTipData: {submittedTip: Tip}) {
    this.tipListingVisible = true;
    this.tipFormVisible = false;
    this.showTipsButtonDisabled = true;
    this.addNewButtonDisabled = false;
  }
}
